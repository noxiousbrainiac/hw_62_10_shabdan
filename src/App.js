import React from 'react';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import MyPortfolio from "./containers/MyPortfolio/MyPortfolio";
import FastFoodApp from "./containers/FastFoodApp/FastFoodApp";
import FindCountryApp from "./containers/FindCountryApp/FindCountryApp";
import Messenger from "./containers/MessengerApp/MessengerApp";

const App = () => {
    return (
        <div>
            <BrowserRouter>
                <Switch>
                    <Route path="/" exact component={MyPortfolio}/>
                    <Route path="/fastfoodapp" component={FastFoodApp}/>
                    <Route path="/findcountryapp" component={FindCountryApp}/>
                    <Route path="/messenger" component={Messenger}/>
                </Switch>
            </BrowserRouter>
        </div>
    );
};

export default App;