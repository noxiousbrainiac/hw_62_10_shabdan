import React from 'react';
import '../../assets/bootstrap.min.css';

const Navigation = () => {
    return (
        <div className="container">
            <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
                <a className="navbar-brand" href="/">My Portfolio</a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
                        aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"> </span>
                </button>
                <div className="collapse navbar-collapse">
                    <div className="navbar-nav">
                        <a className="nav-item nav-link active" href="/">Home</a>
                        <a className="nav-item nav-link" href="/fastfoodapp">Fast-food</a>
                        <a className="nav-item nav-link" href="/findcountryapp">Find-country</a>
                        <a className="nav-item nav-link" href="/messenger">Messenger</a>
                    </div>
                </div>
            </nav>
        </div>
    );
};

export default Navigation;