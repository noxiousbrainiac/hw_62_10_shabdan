import React from 'react';
import {Carousel} from "react-bootstrap";
import '../../assets/bootstrap.min.css';
import firstPic from '../../assets/firstPic.png';
import secondPic from '../../assets/secondPic.png';
import thirdPic from '../../assets/thirdPic.png';

const MyCarousel = () => {
    return (
        <Carousel fade variant="dark">
            <Carousel.Item>
                <img
                    className="d-block w-100"
                    src={firstPic}
                    alt="First slide"
                />
                <Carousel.Caption>
                    <h5>Fast-food app</h5>
                </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
                <img
                    className="d-block w-100"
                    src={secondPic}
                    alt="Second slide"
                />
                <Carousel.Caption>
                    <h5>Find-country app</h5>
                </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
                <img
                    className="d-block w-100"
                    src={thirdPic}
                    alt="Third slide"
                />
                <Carousel.Caption>
                    <h5>Messenger</h5>
                </Carousel.Caption>
            </Carousel.Item>
        </Carousel>
    );
};

export default MyCarousel;