import React from 'react';
import SingleMessage from "../SingleMessage/SingleMessage";
import '../../../assets/bootstrap.min.css';

const ReadMessages = ({messages}) => {
    return (
        <div className="p-3" style={{height: "650px", overflowY: "scroll"}}>
            {messages.map((mes, index) => (
                <SingleMessage key={index} author={mes.author} text={mes.message} time={mes.datetime}/>
            ))}
        </div>
    );
};

export default ReadMessages;