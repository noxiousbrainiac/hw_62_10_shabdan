import {useState} from 'react';
import {nanoid} from "nanoid";
import Orders from "../../components/FastFoodComponents/Orders/Orders";
import AddOrder from "../../components/FastFoodComponents/AddOrder/AddOrder";
import '../../assets/bootstrap.min.css';
import cola from '../../assets/cola.png';
import coffee from '../../assets/coffee.png';
import donut from '../../assets/donut.png';
import fries from '../../assets/fries.png';
import hamburger from '../../assets/hamburger.png';
import doner from '../../assets/doner.png';
import pizza from '../../assets/pizza.png';
import chicken from '../../assets/chicken.png';
import Navigation from "../../components/Navigation/Navigation";

const FastFoodApp = () => {
    const orderMenu = [
        {name: "Cola", price: 30, img: cola},
        {name: "Coffee", price: 30, img: coffee},
        {name: "Donut", price: 50, img: donut},
        {name: "Fries", price: 60, img: fries},
        {name: "Hamburger", price: 100, img: hamburger},
        {name: "Doner-Kebab", price: 120, img: doner},
        {name: "Pizza", price: 290, img: pizza},
        {name: "Chicken", price: 320, img: chicken}
    ];

    const [orders, setOrders] = useState([
        {name: "Cola", count: 0, id: nanoid()},
        {name: "Coffee", count: 0, id: nanoid()},
        {name: "Donut", count: 0, id: nanoid()},
        {name: "Fries", count: 0, id: nanoid()},
        {name: "Hamburger", count: 0, id: nanoid()},
        {name: "Doner-Kebab", count: 0, id: nanoid()},
        {name: "Pizza", count: 0, id: nanoid()},
        {name: "Chicken", count: 0, id: nanoid()},
    ]);

    const addOrder = name => {
        setOrders(orders.map(order => {
            if (order.name === name) {
                return {
                    ...order,
                    count: order.count + 1
                }
            }
            return order;
        }))
    };

    const removeOrder = name => {
        setOrders(orders.map(order => {
            if (order.name === name) {
                return {
                    ...order,
                    count: 0
                }
            }
            return order;
        }))
    };
    return (
        <>
            <div className="bg-primary">
                <Navigation/>
            </div>
            <div className="bg-success py-2">
                <div className="d-flex p-1 justify-content-evenly">
                    <Orders
                        orders={orders}
                        orderMenu={orderMenu}
                        removeOrder={removeOrder}
                    />
                    <AddOrder orderMenu={orderMenu} addOrder={addOrder}/>
                </div>
            </div>
        </>

    );
};

export default FastFoodApp;