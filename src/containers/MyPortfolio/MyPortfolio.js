import React from 'react';
import Navigation from "../../components/Navigation/Navigation";
import '../../assets/bootstrap.min.css';
import MyCarousel from "../../components/MyCarousel/MyCarousel";

const MyPortfolio = () => {
    return (
        <>
            <div className="bg-primary">
                <Navigation/>
            </div>
            <div className="bg-gradient bg-success">
                <div className="container">
                    <MyCarousel/>
                </div>
            </div>
        </>

    );
};

export default MyPortfolio;