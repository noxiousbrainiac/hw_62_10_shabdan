import React, {useEffect, useState} from 'react';
import axios from "axios";
import CountriesList from "../../components/FindCountryAppComponents/CountriesList/CountriesList";
import CountryInfo from "../../components/FindCountryAppComponents/CountryInfo/CountryInfo";
import '../../assets/bootstrap.min.css';
import Navigation from "../../components/Navigation/Navigation";

const url = 'https://restcountries.eu/rest/v2/all?fields=name;alpha3Code';

const CountryInfoApp = () => {
    const [countries, setCountries] = useState([]);
    const [country, setCountry] = useState([]);

    const getAxios = async () => {
        const {data} = await axios.get(url);
        setCountries(data);
    }

    const getCountry = async (code) => {
        const {data} = await axios.get(`https://restcountries.eu/rest/v2/alpha/${code}`);
        setCountry(data);
    }

    useEffect(() => {
        getAxios();
    }, []);

    return (
        <>
            <div className="bg-primary">
                <Navigation/>
            </div>
            <div className="bg-light">
                <div className="container d-flex justify-content-evenly">
                    <CountriesList countries={countries} getCountry={getCountry}/>
                    <CountryInfo country={country}/>
                </div>
            </div>
        </>
    );
};

export default CountryInfoApp;